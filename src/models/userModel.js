const bcrypt=require('bcrypt');
let SALT =10;
//hashing the password

userschema.pre('save',function(next){
  var user =this;
  if(user.isModified('password')){
    bcrypt.genSalt(SALT,function(err,salt){
      if(err)return next(err);

      bcrypt.hash(user.password,salt,function(err,hach){
        if(err)returnnext(err);
        user.password=hash;
        next();
      })
    })
  }else {
    next()
  }
})
//comparing password
userschema.methods.comparePassword=function(candidatePassword,chechpassword){
  bcrypt.compare(candidatePassword,this.password,function(err,isMatch)){
    if(err)return chechpassword(err)
    chechpassword(null,isMatch)
  })
}


module.exports = function (mongoose) {
  var Schema = mongoose.Schema;
  var userschema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
        trim: true
      },
      username: {
        type: String,
        unique: true,
        required: true,
        trim: true
      },
      password: {
        type: String,
        required: true,
      }
    });
  return mongoose.model("usermodel", userschema, "users");
};
