module.exports = function (mongoose) {
  var Schema = mongoose.Schema;
  var Bookschema = new Schema({

    code:String,
    title:String,
    author:String,
    language:String,
    publicationDate:Date,
    empruntable:Boolean,

    type: String,
  });
  return mongoose.model("Bookmodel", Bookschema, "Book");
};
