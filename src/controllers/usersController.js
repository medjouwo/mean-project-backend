var mongoose = require("mongoose");
Movie = require("../models/usersModel.js")(mongoose);

exports.list_users = function (req, res) {
  Movie.find({}, function (err, movie) {
    if (err) res.send(err);
    res.json(movie);
  });
};

exports.read_movie = function (req, res) {
  Movie.findById(req.params.id, function (err, movie) {
    if (err) res.send(err);
    else {
      if (movie == null) {
        res.status(404).send({
          description: "Movie not found",
        });
      } else {
        res.json(movie);
      }
    }
  });
};

exports.create_movie = function (req, res) {
  var new_movie = new Movie(req.body);
  new_movie.save(function (err, movie) {
    if (err) res.send(err);
    res.status(201).json(movie);
  });
};

exports.update_movie = function (req, res) {
  Movie.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    function (err, movie) {
      if (err) res.send(err);
      else {
        if (movie == null) {
          res.status(404).send({
            description: "Movie not found",
          });
        } else {
          res.json(movie);
        }
      }
    }
  );
};

exports.delete_movie = function (req, res) {
  Movie.deleteOne({ _id: req.params.id }, function (err, result) {
    if (err) res.send(err);
    else {
      if (result.deletedCount == 0) {
        res.status(404).send({
          description: "Movie not found",
        });
      } else {
        res.json({ message: "Task successfully deleted" });
      }
    }
  });
};

exports.queryDB = function (req, res) {
  var fromyear = req.query.fromyear;
  var toyear = req.query.toyear;
  var actor = req.query.actor;
  Movie.aggregate(
    [
      {
        $match: {
          $and: [
            { year: { $gte: 1 * fromyear, $lte: 1 * toyear } },
            { actors: actor },
          ],
        },
      },
      { $sort: { year: 1 } },
    ],
    function (err, movie) {
      if (err) res.send(err);
      else {
        if (movie.length <= 0) {
          res.status(404).send({
            description: "Movie not found",
          });
        } else {
          res.json(movie);
        }
      }
    }
  );
};
