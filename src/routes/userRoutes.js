module.exports = function (app) {
  var usersController = require("../controllers/usersController");

  app
    .route("/users")
    .get(usersController.list_users)
    .post(usersController.create_users);

  app
    .route("/users/:id")
    .get(usersController.read_movie)
    .put(usersController.update_movie)
    .delete(usersController.delete_movie);


  app.route("/query").get(usersController.queryDB);

};
